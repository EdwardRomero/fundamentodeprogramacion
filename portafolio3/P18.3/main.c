#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//dato de cada elemento
typedef struct{
	int coeficiente,exponente;
}termino;

//estructura de los nodos de la lista
struct elemento{
	termino data;
	struct elemento *enlace;
};
typedef struct elemento nodo;

int insertarNodo(nodo**);
void mostrarFuncion(nodo *cabeza);
void mostrarResultadosPolinomio(nodo*);

main(){
	nodo *cabeza=NULL;

	printf("Este programa permite dar entrada a polinomios en x,\nrepresentandolos como una lista enlazada.\n\n");

	printf("Indique los terminos del polinomio (escriba exponente -1 para terminar):\n\n");

	int i=1;
	do{
		printf("\nTermino %d:\n",i++);
	}while(insertarNodo(&cabeza));

	mostrarFuncion(cabeza);

	printf("\nSalidas de f(x) en [0,5]:\n");
	mostrarResultadosPolinomio(cabeza);
}

void mostrarResultadosPolinomio(nodo *cabeza){
	nodo *indice;
	float x,total,parcial;

	printf("x\tf(x)\n");
	printf("-------------\n");

	for(x=0;x<=5;x+=0.5){
		total=0;
		for(indice=cabeza;indice;indice=indice->enlace){
			parcial=pow(x,indice->data.exponente);
			parcial*=indice->data.coeficiente;
			//procesar salida con x
			total+=parcial;
		}
		printf("%.1f\t%.2f\n",x,total);
	}
}

void mostrarFuncion(nodo *cabeza){
	nodo *indice;
	printf("\nf(x)=");
	for(indice=cabeza;indice;indice=indice->enlace){
		if(indice->enlace!=NULL){
			printf("(%dx^%d)+",indice->data.coeficiente,indice->data.exponente);
		}
		else{
			printf("(%dx^%d)\n",indice->data.coeficiente,indice->data.exponente);
		}
	}
}

int insertarNodo(nodo **cabeza){
    nodo *nuevo=(nodo*)malloc(sizeof(nodo));

	termino tmp;
	printf("\tCoeficiente: ");
	scanf("%d",&tmp.coeficiente);
	printf("\tExponente: ");
	scanf("%d",&tmp.exponente);

    nuevo->data=tmp;
    if(nuevo->data.exponente>-1){
		nuevo->enlace=*cabeza;
		*cabeza=nuevo;
		return 1;
    }
    else{
		return 0;
    }
}
