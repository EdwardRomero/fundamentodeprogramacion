#include <stdio.h>
#include <stdlib.h>

#define NOMBRE "test.txt"

int main(void)
{
   FILE *entrada;
   int ch, num_lineas=0;

   entrada = fopen(NOMBRE, "r")

   while ((ch = fgetc(entrada)) != EOF)
      if (ch == '\n')
         num_lineas++;

   fclose(entrada);
   printf("Numero de lineas: %d\n", num_lineas);

   return;
}
