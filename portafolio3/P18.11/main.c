#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//estructura de los nodos de la lista
struct elemento{
	int data;
	struct elemento *enlace;
};
typedef struct elemento nodo;

int contarElementos(nodo*);
int insertarNodo(nodo**);
void recorrerRec(nodo*);
void sumar(nodo**,nodo*,nodo*);

main(){
    //crear la cabeza del primer numero (primera lista)
	nodo *cabeza1=(nodo*)malloc(sizeof(nodo));
	cabeza1->data=-1;
	cabeza1->enlace=cabeza1;
	//crear la cabeza del segundo numero (segunda lista)
	nodo *cabeza2=(nodo*)malloc(sizeof(nodo));
	cabeza2->data=-1;
	cabeza2->enlace=cabeza2;
	//crear la cabeza del numero resultante de la suma (tercera lista)
	nodo *cabeza3=(nodo*)malloc(sizeof(nodo));
	cabeza3->data=-1;
	cabeza3->enlace=cabeza3;

	printf("Este programa permite representar numeros largos como una lista enlazada.\n\n");

    printf("Numero 1: ");
    while(insertarNodo(&cabeza1));
    printf("Numero 2: ");
    while(insertarNodo(&cabeza2));

    printf("\n\n");

    recorrerRec(cabeza1);
    printf(" + ");
    recorrerRec(cabeza2);

    sumar(&cabeza3,cabeza1,cabeza2);

    printf("\n\n");
    recorrerRec(cabeza3);
}

int contarElementos(nodo *cabeza){
    nodo *indice;
    int i=0;

    for(indice=cabeza->enlace;indice->data!=-1;indice=indice->enlace){
        i++;
    }

    return i;
}

int insertarNodo(nodo **cabeza){
    nodo *indice,*nuevo=(nodo*)malloc(sizeof(nodo));

    int x=getche();
    if(x>='0' && x<='9'){
        x-='0';
        nuevo->data=x;

		nuevo->enlace=*cabeza;
		*cabeza=nuevo;
		return 1;
    }
    else if(x==13){
        printf("\n\n");
        return 0;
    }

    return 1;
}

void recorrerRec(nodo *cabeza){
    if(cabeza->data==-1){
        return;
    }
    recorrerRec(cabeza->enlace);

    printf("%d",cabeza->data);
}

void sumar(nodo **salida,nodo *a,nodo *b){
    nodo *nuevo,*inA=a,*inB=b;
    int suma,sobra=0;

    if(contarElementos(a) >= contarElementos(b)){
        for(; inA->data!=-1; inA=inA->enlace,inB=inB->enlace){
            suma=sobra;

            nuevo=(nodo*)malloc(sizeof(nodo));

            if(inB->data!=-1){
                suma+=(inA->data + inB->data);
                if(suma<10){
                    nuevo->data = suma;
                    nuevo->enlace=*salida;
                    *salida=nuevo;
                }
                else{
                    sobra=suma/10;
                    suma-=10;

                    nuevo->data = suma;
                    nuevo->enlace = *salida;
                    *salida=nuevo;
                }
            }
            else{

                suma += inA->data;
                nuevo->data = suma;

                nuevo->enlace = *salida;
                *salida=nuevo;

                sobra=0;
            }
        }
    }
    else{
        for(inB=b; inB->data!=-1; inB=inB->enlace,inA=inA->enlace){
            suma=sobra;

            nuevo=(nodo*)malloc(sizeof(nodo));

            if(inA->data!=-1){
                suma+=(inA->data + inB->data);
                if(suma<10){
                    nuevo->data = suma;
                    nuevo->enlace=*salida;
                    *salida=nuevo;
                }
                else{
                    sobra=suma/10;
                    suma-=10;

                    nuevo->data = suma;
                    nuevo->enlace = *salida;
                    *salida=nuevo;
                }
            }
            else{

                suma += inB->data;
                nuevo->data = suma;

                nuevo->enlace = *salida;
                *salida=nuevo;

                sobra=0;
            }
        }
    }
}
