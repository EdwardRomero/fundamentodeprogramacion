#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct elemento{
    int dato;
    struct elemento *siguiente;
};
typedef struct elemento nodo;

void crearLista(nodo**);
void eliminarNodos(nodo**);
void imprimirLista(nodo*);
void insertarNodo(nodo **cabeza);

main(){
    nodo *cabeza=NULL;

    printf("Este programa permita crear una lista enlazada de numeros aleatorios\ny eliminar los numeros que esten por encima de un valor dado.\n\n");

    ///creacion de la lista
    crearLista(&cabeza);
    //impresion de la lista con todos los elementos
    imprimirLista(cabeza);

    ///eliminacion de la posicion deseada de la lista
    eliminarNodos(&cabeza);
    //impresion de la lista con los nuevos elementos
    imprimirLista(cabeza);
}

void crearLista(nodo **cabeza){
    int i,n;

    printf("\n\tIndique cuantos elementos desea en la lista: ");
    scanf("%d",&n);

    for(i=0;i<n;i++){
        insertarNodo(cabeza);
    }
}

void eliminarNodos(nodo **cabeza){
    nodo *actual,*anterior,*tmp;
    int i;
    printf("\n\n\tDigite el valor maximo para los elementos de la lista: ");
    scanf("%d",&i);
    for(anterior=actual=*cabeza; actual!=NULL;anterior=actual,actual=actual->siguiente){
        if(actual->dato > i){
            if(actual==*cabeza){
                *cabeza = actual->siguiente;
            }
            else{
                anterior->siguiente = actual->siguiente;
            }
        }
    }
}

void imprimirLista(nodo *cabeza){
    nodo *indice;
    for(indice=cabeza; indice; indice=indice->siguiente){
        printf("%d\t",indice->dato);
    }
}

void insertarNodo(nodo **cabeza){
    nodo *indice,*nuevo = (nodo*)malloc(sizeof(nodo));

    int x=rand()%67;
    nuevo->dato = x;
    nuevo->siguiente = NULL;

    if(!(*cabeza)){
        *cabeza=nuevo;
    }
    else{
        for(indice=*cabeza; indice->siguiente; indice=indice->siguiente);
        indice->siguiente = (nodo*)malloc(sizeof(nodo));
        indice->siguiente = nuevo;
    }
}
