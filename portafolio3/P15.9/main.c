#include <stdio.h>
#include <stdio.h>
#include <time.h>

#define MAXMIN 120

main(){
    FILE *archivo;

    struct tm *fecha; //variable para el tiempo
    time_t hora; //tiempo en segundos

    time(&hora);
    fecha=localtime(&hora);

    char nombreArchivo[40]; //ruta del archivo

    /*Funci�n para construir la ruta a partir de la variable "fecha"*/
    strftime(nombreArchivo, 38, "REGISTRO %m_%d_%Y   %I.%M.%p.txt", fecha);

    archivo=fopen(nombreArchivo,"wt");

    int minutos,i;

    do{
        printf("Digite el tiempo de entrenamiento (en minutos): ");
        scanf("%d",&minutos);
    }while(minutos<=0);

    char header[100];

    strftime(header,100,"FECHA: %d/%m/%Y      HORA: %I:%M %p",fecha);

    fprintf(archivo,header);
    fprintf(archivo,"      DURACION: %d minutos\n\n\n",minutos);

    srand(time(NULL));

    for(i=0;i<=minutos*60;i++){
        if(i%15==0 && i>0){
            fprintf(archivo,"Toma parcial: %d\tPulsaciones:%d\n",i,19+rand()%169);
        }
    }

    fclose(archivo);

    return 0;
}
