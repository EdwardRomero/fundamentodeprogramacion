#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

main(int nArg, char *arg[]){
    DIR *directorio;
    struct dirent *iArchivo;

    if((directorio=opendir(arg[1]))==NULL){
        printf("DIRECTORIO NO ENCONTRADO");
        return 0;
    }
    else{
        while((iArchivo=readdir(directorio))!=NULL){
            printf("%s\n",iArchivo->d_name);
        }
    }

    return 1;
}
