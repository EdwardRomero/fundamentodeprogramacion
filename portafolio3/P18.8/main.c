#include <stdio.h>
#include <stdlib.h>

struct elemento{
    int dato;
    struct elemento *enlace;
};
typedef struct elemento nodo;

void agregarElemento(nodo**, int);
void borrarLista(nodo**);
void diferencia(nodo*,nodo*,nodo**);
void imprimirConjunto(nodo*);
int inclusion(nodo *A,nodo *B);
void interseccion(nodo *A, nodo *B, nodo **C);
int pertenencia(nodo*, int);
void unirConjuntos(nodo *A, nodo *B, nodo **C);

int main(){
    nodo *cabezaA,*cabezaB,*cabezaC;
    cabezaA = cabezaB = cabezaC = NULL;
    int opcion, elemento;
    char conjuntoElegido;

    do{
        system("CLS");
        printf("Este programa maneja conjuntos de enteros como listas enlazadas,\npueden hacerse las siguientes operaciones sobre un conjunto:\n\n");
        printf("\t1) Agregar elemento a un conjunto disponible\n");
        printf("\n\t2) Unir conjuntos disponibles\n");
        printf("\t3) Interseccion de los conjuntos disponibles\n");
        printf("\t4) Diferencia de los conjuntos\n");
        printf("\t5) Inclusion de los conjuntos\n");
        printf("\n\t6) Imprimir elementos del conjunto\n");
        printf("\t7) Salir\n\n");

        printf("Indique su opcion: ");

        do{
            scanf("%d", &opcion);
            fflush(stdin);

            if(opcion<1 || opcion>7){
                printf("ERROR! ESCOJA UNA  OPCION VALIDA\n");
            }
        }while(opcion<1 || opcion >7);

        system("CLS");
        switch(opcion){
        ///agregar el elemento digitado al conjunto
            case 1:
                printf("Digite el dato: ");
                scanf("%d", &elemento);

                fflush(stdin);
                do{
                    printf("\nEn cual conjunto deseas insertarlo? A o B? ");
                    conjuntoElegido=tolower(getche());
                }while(conjuntoElegido!='a' && conjuntoElegido!='b');

                conjuntoElegido=='a'? agregarElemento(&cabezaA, elemento) : agregarElemento(&cabezaB, elemento);
                system("PAUSE");
                break;

        ///unir conjuntos A y B
            case 2:
                if(!cabezaA && !cabezaB){
                    printf("Todos los conjuntos disponibles estan vacios\n\n");
                }
                else{
                    borrarLista(&cabezaC);
                    unirConjuntos(cabezaA,cabezaB,&cabezaC);
                    printf("Union de A y B:\n");
                    imprimirConjunto(cabezaC);
                }
                system("PAUSE");
                break;

        ///interseccion de los conjuntos A y B
            case 3:
                if(!cabezaA && !cabezaB){
                    printf("Todos los conjuntos disponibles estan vacios\n\n");
                }
                else{
                    borrarLista(&cabezaC);
                    interseccion(cabezaA,cabezaB,&cabezaC);
                    printf("Interseccion de A y B:\n");
                    imprimirConjunto(cabezaC);
                }
                system("PAUSE");
                break;
        ///diferencia de A y B
            case 4:
                if(!cabezaA && !cabezaB){
                    printf("Todos los conjuntos disponibles estan vacios\n\n");
                }
                else{
                    borrarLista(&cabezaC);
                    diferencia(cabezaA,cabezaB,&cabezaC);
                    printf("Diferencia de A y B (A-B):\n");
                    imprimirConjunto(cabezaC);
                }
                system("PAUSE");
                break;

        ///determinar si B es subconjunto de A
            case 5:
                if(!cabezaA && !cabezaB){
                    printf("Todos los conjuntos disponibles estan vacios\n\n");
                }
                else{
                    if(inclusion(cabezaA,cabezaB)){
                        printf("B es subconjunto de A\n\n");
                    }
                    else{
                        printf("B no esta contenido dentro de A\n\n");
                    }
                }
                system("PAUSE");
                break;

        ///mostrar miembros actuales de cada conjunto
            case 6:
                //A
                printf("Elementos de A:\n");
                if(cabezaA == NULL){
                    printf("\tEl conjunto A vacio. Agregue elementos al conjunto\n\n");
                }
                else{
                    imprimirConjunto(cabezaA);
                }
                //B
                printf("Elementos de B:\n");
                if(cabezaB == NULL){
                    printf("\tEl conjunto B vacio. Agregue elementos al conjunto\n\n");
                }
                else{
                    imprimirConjunto(cabezaB);
                }
                system("PAUSE");
                break;
        }

    }while(opcion != 7);

    return 0;
}

void agregarElemento(nodo **cabeza, int num){
    nodo *nuevo,*indice;

    nuevo = (nodo*)malloc(sizeof(nodo));
    nuevo->dato = num;
    nuevo->enlace = NULL;

    if((*cabeza)!=NULL && (*cabeza)->dato == num){
        printf("\n\nElemento que indico ya es miembro del conjunto, \ny no pueden haber duplicados!\n\n");
        return;
    }
    if(!(*cabeza)){
        *cabeza=nuevo;
        (*cabeza)->enlace = NULL;
    }
    else{
        for(indice=*cabeza; indice->enlace; indice=indice->enlace){
            if(indice->dato == num){
                printf("\n\nElemento que indico ya es miembro del conjunto, \ny no pueden haber duplicados!\n\n");
                return;
            }
        }

        indice->enlace = nuevo;
    }
    printf("\n\tElemento a%cadido exitosamente!\n\n", 164);
}

void borrarLista(nodo **cabeza){
    nodo *tmp;

    while(*cabeza){
        tmp = *cabeza;
        *cabeza = (*cabeza)->enlace;
        free(tmp);
    }
}

void diferencia(nodo *A, nodo *B, nodo **C){
    nodo *indiceC,*nuevo;
    indiceC=nuevo=NULL;

    while(A){
        if(pertenencia(B,A->dato)){
            A = A->enlace;
            continue;
        }
        nuevo = (nodo*)malloc(sizeof(nodo));
        nuevo->dato = A->dato;
        nuevo->enlace = NULL;

        if(!*C){
            *C = nuevo;
            indiceC = *C;
        }
        else{
            indiceC->enlace = nuevo;
            indiceC = indiceC->enlace;
        }

        A = A->enlace;
    }
}

void imprimirConjunto(nodo* cabeza){
    nodo* indice;

    for(indice=cabeza; indice; indice=indice->enlace){
        printf("%d\t", indice->dato);
    }
    printf("\n\n");
}

int inclusion(nodo *A,nodo *B){
    nodo *indice;
    for(indice=B; indice; indice=indice->enlace){
        if(!pertenencia(A,indice->dato)){
            return 0;
        }
    }
    //verificar si A esta dentro de B

    return 1;
}

void interseccion(nodo *A, nodo *B, nodo **C){
    nodo *indiceC,*nuevo;
    indiceC=nuevo=NULL;

    while(A){
        if(!pertenencia(B,A->dato)){
            A = A->enlace;
            continue;
        }
        nuevo = (nodo*)malloc(sizeof(nodo));
        nuevo->dato = A->dato;
        nuevo->enlace = NULL;

        if(!*C){
            *C = nuevo;
            indiceC = *C;
        }
        else{
            indiceC->enlace = nuevo;
            indiceC = indiceC->enlace;
        }

        A = A->enlace;
    }
}

int pertenencia(nodo *cabeza, int valor){
    nodo *indice;
    for(indice=cabeza; indice; indice=indice->enlace){
        if(indice->dato == valor){
            return 1;
        }
    }

    return 0;
}

void unirConjuntos(nodo *A, nodo *B, nodo **C){
    nodo *indiceC,*nuevo;
    indiceC=nuevo=NULL;

    while(A){
        if(pertenencia(*C,A->dato)){
            A = A->enlace;
            continue;
        }
        nuevo = (nodo*)malloc(sizeof(nodo));
        nuevo->dato = A->dato;
        nuevo->enlace = NULL;

        if(!*C){
            *C = nuevo;
            indiceC = *C;
        }
        else{
            indiceC->enlace = nuevo;
            indiceC = indiceC->enlace;
        }

        A = A->enlace;
    }

    while(B){
        if(pertenencia(*C,B->dato)){
            B = B->enlace;
            continue;
        }
        nuevo = (nodo*)malloc(sizeof(nodo));
        nuevo->dato = B->dato;
        nuevo->enlace = NULL;

        if(!*C){
            *C = nuevo;
            indiceC = *C;
        }
        else{
            indiceC->enlace = nuevo;
            indiceC = indiceC->enlace;
        }

        B = B->enlace;
    }
}
