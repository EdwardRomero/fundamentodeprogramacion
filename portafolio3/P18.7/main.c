#include <stdio.h>
#include <stdlib.h>

struct elemento{
    int dato;
    struct elemento *enlace;
};
typedef struct elemento nodo;

void agregarElemento(nodo**, int);
int cardinal(nodo*);
void imprimirConjunto(nodo*);
int pertenencia(nodo*, int);

int main(){
    nodo *cabeza = NULL;
    int opcion, elemento;

    do{
        system("CLS");
        printf("Este programa maneja conjuntos de enteros como listas enlazadas,\npueden hacerse las siguientes operaciones sobre un conjunto:\n\n");
        printf("\t1) Determinar cardinal del conjunto\n");
        printf("\t2) Pertenencia de un elemento al conjunto\n");
        printf("\t3) Agregar elemento al conjunto\n");
        printf("\t4) Imprimir elementos del conjunto\n");
        printf("\t5) Salir\n\n");

        printf("Indique su opcion: ");

        do{
            scanf("%d", &opcion);
            fflush(stdin);

            if(opcion<1 || opcion>5){
                printf("ERROR! ESCOJA UNA  OPCION VALIDA\n");
            }
        }while(opcion<1 || opcion >5);

        system("CLS");
        switch(opcion){
            case 1:
                //contar cuantos elementos hay en el conjunto
                printf("Cardinal del conjunto: %d\n\n", cardinal(cabeza));
                system("PAUSE");
                break;

            case 2:
                //verificar si el elemento digitado esta contenido en el conjunto
                printf("Digite un elemento: ");
                scanf("%d", &elemento);
                fflush(stdin);

                if(pertenencia(cabeza, elemento)){
                    printf("\n%d pertenece al conjunto.\n\n", elemento);
                }
                else{
                    printf("\n%d NO pertenece al conjunto.\n\n", elemento);
                }
                system("PAUSE");
                break;

            case 3:
                //agregar el elemento digitado al conjunto
                printf("Digite un elemento: ");
                scanf("%d", &elemento);
                fflush(stdin);

                agregarElemento(&cabeza, elemento);
                system("PAUSE");
                break;

            case 4:
                //mostrar miembros actuales del conjunto
                if(cabeza == NULL){
                    printf("El conjunto esta vacio. Agregue elementos al conjunto\n\n");
                }
                else{
                    printf("Elementos del conjunto:\n");
                    imprimirConjunto(cabeza);
                }
                system("PAUSE");
                break;
        }

    }while(opcion != 5);

    return 0;
}

int cardinal(nodo *cabeza){
    nodo *indice;
    int n = 0;

    for(indice=cabeza; indice; indice=indice->enlace){
        n++;
    }

    return n;
}

int pertenencia(nodo *cabeza, int valor){
    nodo *indice;
    for(indice=cabeza; indice; indice=indice->enlace){
        if(cabeza->dato == valor){
            return 1;
        }
    }

    return 0;
}

void agregarElemento(nodo **cabeza, int num){
    nodo *nuevo,*indice;

    nuevo = (nodo*)malloc(sizeof(nodo));
    nuevo->dato = num;
    nuevo->enlace = NULL;

    if((*cabeza)!=NULL && (*cabeza)->dato == num){
        printf("\nElemento que indico ya es miembro del conjunto, \ny no pueden haber duplicados!\n\n");
        return;
    }
    if(!(*cabeza)){
        *cabeza=nuevo;
        (*cabeza)->enlace = NULL;
    }
    else{
        for(indice=*cabeza; indice->enlace; indice=indice->enlace){
            if(indice->dato == num){
                printf("\nElemento que indico ya es miembro del conjunto, \ny no pueden haber duplicados!\n\n");
                return;
            }
        }

        indice->enlace = nuevo;
    }
    printf("\nElemento a%cadido exitosamente!\n\n", 164);
}

void imprimirConjunto(nodo* cabeza){
    nodo* indice;

    for(indice=cabeza; indice; indice=indice->enlace){
        printf("%d\t", indice->dato);
    }
    printf("\n\n");
}
