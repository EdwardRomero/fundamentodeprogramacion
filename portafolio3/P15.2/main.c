#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int dia;
    int mes;
    int anyo;
}ffecha;

typedef struct
{
    int hora;
    int minutos;
}ttiempo;

typedef struct
{
    ffecha fecha;
    ttiempo tiempo;
    int minu;
    int pulsaciones;
}ccorredor;

int main()
{

    FILE *pfile = fopen("corredores.txt","w+");
    printf("este programa sirve para registrar datos de un pulsometro\n");
    printf("Registrando los datos de 1 corredor ");
    ccorredor corredor;
    ccorredor *pcorredor= &corredor;

    printf("\ndia: ");
    scanf("%d",&corredor.fecha.dia);
    printf("mes: ");
    scanf("%d",&corredor.fecha.mes);
    printf("anio: ");
    scanf("%d",&corredor.fecha.anyo);
    printf("hora: ");
    scanf("%d",&corredor.tiempo.hora);
    printf("minutos: ");
    scanf("%d",&corredor.tiempo.minutos);
    printf("tiempo: ");
    scanf("%d",&corredor.minu);
    printf("pulsaciones: ");
    scanf("%d",&corredor.pulsaciones);

    fwrite(pcorredor,sizeof corredor,1,pfile);
    printf("hola");
    fread(pcorredor,sizeof corredor,1,pfile);
    return 0;
}
