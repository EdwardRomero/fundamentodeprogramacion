#include <stdio.h>
#include <stdlib.h>

typedef struct hola
{
    int *dato;
    struct hola *siguiente;
}llista;

int contador(llista *lista);

int main()
{
    llista *lista = malloc(sizeof(llista));
    printf("Hello world!\n");
    contador(&lista);
    return 0;
}

int contador(llista *lista)
{
    int i;
    llista nodoActual = *lista;
    for(i=0;nodoActual.siguiente!=NULL;nodoActual=nodoActual.siguiente)
        i++;
    return i;
}
