#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int cod;
    char autor[100];
    char titulo[200];
    float precioCompra;
    float precioVenta;
}libro;

int ultimoID=0;
FILE *archivo;

void agregarLibro();
void capturarDatosLibro(libro*);
void mostrarLibro();
void cambiarPrecioVenta();

main(){
    int salir=0;

    if((archivo=fopen("liber.dat","w+b"))==NULL){
        printf("ERROR AL ABRIR COLECCION DE LIBROS");
        return -1;
    }

    do{
        system("CLS");
        printf("\nEste programa permita manejar informacion sobre libros,\ncontiene las siguientes funcionalidades:\n");
        printf("\t1)Agregar libros\n");
        printf("\t2)Buscar libro por codigo\n");
        printf("\t3)Cambiar precio de libro\n");
        printf("\n\t4)Salir\n");
        printf("\n\tSeleccione: ");

        fflush(stdin);
        switch(getche()){
            case '1':
                agregarLibro();
                break;
            case '2':
                mostrarLibro();
                break;
            case '3':
                cambiarPrecioVenta();
                break;
            case '4':
                salir=1;
                break;
        }

    }while(!salir);

    return 0;
}

void agregarLibro(){
    system("CLS");
    libro _tmp;

    //si el archivo esta vacio, crea el primer ID
    if(ftell(archivo)==0){
        ultimoID=0;
    }
    else{
        //mover el cursor hasta antes del ultimo libro
        //para asignar su ID como el ultimoID
        fseek(archivo,sizeof(libro)*(ultimoID-1),SEEK_SET);
        fread(&ultimoID,sizeof(int),1,archivo);

        //mover el cursor al final del archivo para guardar
        //el nuevo libro
        fseek(archivo,sizeof(libro)*ultimoID,SEEK_SET);
    }
    _tmp.cod= ++ultimoID;

    //captura de datos restantes del libro
    capturarDatosLibro(&_tmp);

    //guardar en el archivo el libro capturado
    fwrite(&_tmp,sizeof(libro),1,archivo);
}

void cambiarPrecioVenta(){
    do{
        fflush(stdin);
        system("CLS");

        int codigo;
        float p;
        libro x;

        printf("Digite el ID del libro que desea editar: ");
        scanf("%d",&codigo);

        if(codigo>ultimoID){
            printf("ID NO VALIDO\n\n");
        }
        else{
            fseek(archivo,sizeof(libro)*(codigo-1),SEEK_SET);
            fread(&x,sizeof(libro),1,archivo);

            printf("Nuevo precio: ");
            scanf("%f",&p);
            x.precioVenta=p;
            fseek(archivo,sizeof(libro)*(codigo-1),SEEK_SET);

            fwrite(&x,sizeof(libro),1,archivo);
        }

        printf("\n\nSi desea editar otro precio presione s (minuscula)\notra letra para finalizar la edicion: ");
    }while(getch()=='s');
}

void capturarDatosLibro(libro *x){
    printf("Capturando datos de nuevo libro..\n");
    printf("\nID LIBRO: %d\n",x->cod);

    fflush(stdin);
    printf("\n\tAutor: ");
    gets(x->autor);
    printf("\tTitulo: ");
    gets(x->titulo);

    printf("\tPrecio de compra: ");
    scanf("%f",&x->precioCompra);
    printf("\tPrecio de venta: ");
    scanf("%f",&x->precioVenta);
}

void mostrarLibro(){
    do{
        fflush(stdin);
        system("CLS");

        int codigo;
        libro x;

        printf("Digite el ID del libro que desea consultar: ");
        scanf("%d",&codigo);

        if(codigo>ultimoID){
            printf("ID NO VALIDO\n\n");
        }
        else{
            fseek(archivo,sizeof(libro)*(codigo-1),SEEK_SET);

            fread(&x,sizeof(libro),1,archivo);

            printf("\n\n\t");
            printf("#%d\n\t",x.cod);
            printf("%s, escrito por %s \n\tLo compramos a %.2f y lo vendemos a %.2f\n\n",x.titulo,x.autor,x.precioCompra,x.precioVenta);
        }

        printf("\n\nSi desea buscar otro libro presione s (minuscula)\notra letra para finalizar la consulta: ");
    }while(getch()=='s');
}
