#include <stdio.h>
#include <stdlib.h>
#define column 4

void Ordenar (int filas, int columnas, int Arreglo[filas][column],int pivote);


int main()
{
    int i,j,filas;
    printf("Este programa organiza un array con las fechas de nacimiento\n");
    printf("cuantas filas tiene el array? ");
    scanf("%d",&filas);

    int Arreglo[filas][column];
    filas=filas-1;
    for(i=0;i<=filas;i++)
    {
        printf("inserte la %d fecha (en formado DD/MM/YYYY): ",i+1);
        scanf("%d/%d/%d",&Arreglo[i][0],&Arreglo[i][1],&Arreglo[i][2]);
        Arreglo[i][3]=(Arreglo[i][0])+(Arreglo[i][1]*30)+(Arreglo[i][2]*365);
    }
    Ordenar(filas,column,Arreglo, 0);
    for(i=0;i<=filas;i++)
    {
        printf("%d/%d/%d\n",Arreglo[i][0],Arreglo[i][1],Arreglo[i][2]);
    }
}

void Ordenar (int filas, int columnas, int Arreglo[filas][column], int pivote)
{
    if(pivote==filas)
        return;
    int i;
    for(i=pivote;i<=filas;i++)
    {
        if(Arreglo[i][3]<Arreglo[pivote][3])
        {
            int burbuja[column];
            int j;
            for(j=0;j<column;j++)
            {
                burbuja[j]=Arreglo[pivote][j];
                Arreglo[pivote][j]=Arreglo[i][j];
                Arreglo[i][j]=burbuja[j];
            }
        }
    }
    return Ordenar(filas,columnas,Arreglo,pivote+1);
}

