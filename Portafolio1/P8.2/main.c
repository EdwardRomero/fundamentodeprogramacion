#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int Palindromo_(char cadena[], int longitud);
int Palindromo(char cadena[], int limI,int limS);

int main()
{
    int i;
    char cadena[50];
    printf("este programa detecta si una palabra es un palindromo o no\n");

    do
    {
        printf("inserte la cadena de caracteres que desea verificar si es un palindromo\n");
        gets(cadena);

        if(Palindromo_(cadena,strlen(cadena)))
        {
            printf("la cadena insertada es un palindromo");
            break;
        }
        else
            printf("error, la cadena no es un palindromo, intentelo de nuevo\n");

    }while(1);
    return 0;
}

int Palindromo_ (char cadena[], int longitud)
{
    return Palindromo(cadena, 0, longitud-1);
}

int Palindromo(char cadena[], int limI,int limS)
{
    //printf("hola\n");
    if(limI>=limS)
        return 1;

    if(cadena[limI]==' ')
        limI++;
    if(cadena[limS]==' ')
        limS--;

    if(cadena[limI]==cadena[limS])
        return Palindromo(cadena, limI+1, limS-1);
    else
        return 0;
}
