#include <stdio.h>
#include <stdlib.h>

int max_ (int x, int y);
int MaxArray(int Arreglo[], int n);
int main()
{
    int n=5;
    int Arreglo[5]={1,2,3,4,5};

    printf("este programa sirve apra ver el numero mas grande en un arreglo\n");

    printf("%d",MaxArray(Arreglo,n-1));
    return 0;
}

int max_(int x, int y)
{
    if (x>y)
        return x;
    else
        return y;
}

int MaxArray(int Arreglo[], int n)
{
    if(n==1)
        return max_(Arreglo[0],Arreglo[n]);

    return max_(MaxArray(Arreglo,n-1),Arreglo[n]);
}
