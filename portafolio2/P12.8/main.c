#include <stdio.h>
#include <stdlib.h>

enum vvisita
{
    ninguno=0,
    lunes=1,
    martes,
    miercoles,
    jueves,
    viernes,
    sabado,
    domingo
};

typedef struct
{
    int dia;
    int mes;
    int anyol;
}ffecha;

typedef struct
{
    char nombre[10];
    char direccion[10];
    ffecha fechadenac;
    char sexo;
    enum vvisita visita;
    char problemamedico[10];

}ppaciente;

void entradadedatos(ppaciente paciente[], int tamano);
void mostrarpordias(ppaciente paciente[], int tamano);
int main()
{
    printf("este programa ayuda a programar los pacientes de un dia\n");
    printf("inserte la cantidad de pasientes a registar: ");
    int n;
    scanf("%d",&n);
    ppaciente paciente[n];
    entradadedatos(paciente,n);
    fflush(stdin);
    mostrarpordias(paciente,n);

    return 0;
}

void entradadedatos(ppaciente paciente[], int tamano)
{
    int i;
    for(i=0;i<tamano;i++)
    {
        printf("\nescaneando el %d paciente\n",i+1);
        printf("\tinserte el nombre: ");
        scanf("%s",&paciente[i].nombre);
        printf("\tinserte la direccion: ");
        scanf("%s",&paciente[i].direccion);
        printf("\tinserte la fecha de nacimiento (DD/MM/YYYY): ");
        scanf("%d/%d/%d",&paciente[i].fechadenac.dia,&paciente[i].fechadenac.mes,&paciente[i].fechadenac.anyol);
        printf("\tinserte el sexo(M=masculino, F=femenino): ");
        scanf("%c",&paciente[i].sexo);
        printf("\tinserte el dia de visita lunes=1, martes=2, miercoles=3 ,jueves=4 ,viernes=5 ,sabado=6 ,domingo=7\n");
        fflush(stdin);
        scanf("%d",&paciente[i].visita);
        printf("\tinserte el problema medico: ");
        scanf("%s",&paciente[i].problemamedico);

    }
}

void mostrarpordias(ppaciente paciente[], int tamano)
{
    int i, eleccion;
    system("cls");
    printf("que dia de la semana desea consultar?\n ");
    printf("lunes=1, martes=2, miercoles=3 ,jueves=4 ,viernes=5 ,sabado=6 ,domingo=7\n");
    scanf("%d",&eleccion);


    printf("\n\n\n");
    switch(eleccion)
    {
    case 1:
        eleccion==lunes;
        printf("imprimiendo los datos del lunes");
        break;

    case 2:
        eleccion==martes;
        printf("imprimiendo los datos del martes");
        break;

    case 3:
        eleccion==miercoles;
        printf("imprimiendo los datos del miercoles");
        break;

    case 4:
        eleccion==jueves;
        printf("imprimiendo los datos del jueves");
        break;

    case 5:
        eleccion==viernes;
        printf("imprimiendo los datos del viernes");
        break;

    case 6:
        eleccion==sabado;
        printf("imprimiendo los datos del sabado");
        break;

    case 7:
        eleccion==domingo;
        printf("imprimiendo los datos del domingo");
        break;
    }

    printf("\n\n");
    for(i=0;i<tamano;i++)
        if(paciente[i].visita==eleccion)
            printf("%s",paciente[i].nombre);

    return;
}
