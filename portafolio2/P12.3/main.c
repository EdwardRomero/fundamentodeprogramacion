#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int minutos;
    int segundos;
}ttiempo;

typedef struct
{
    char nombre[10];
    int edad;
    int prueba;
    ttiempo tiempo;

}nnadadores;

void ingresar_datos(nnadadores nadadores[], int tamano);
int tiempo_promedioM(nnadadores nadadores[], int tamano);
int tiempo_promedioS(nnadadores nadadores[], int tamano);

int main()
{
    int n;
    printf("este programa lee archivos de un dispositivo estandar\n");
    printf("inserte la cantidad de nadadores que se ingresaran: ");
    scanf("%d",&n);

    nnadadores nadadores[n];
    ingresar_datos(nadadores,n);

    printf("el promedio de los tiempo es %d.%d", tiempo_promedioM(nadadores,n), tiempo_promedioS(nadadores,n));
    return 0;
}

void ingresar_datos(nnadadores nadadores[], int tamano)
{
    int i;
    for(i=0;i<tamano;i++)
    {
        printf("\nentrando los datos del %d nadador:\n",i+1);

        printf("\tinserte el nombre: ");
        scanf("%s",&nadadores[i].nombre);
        printf("\tinserte la edad: ");
        scanf("%d",&nadadores[i].edad);
        printf("\tinserte la prueba en que partisipa(0=50m,1=100m,2=150m,3=200m)");
        scanf("%d",&nadadores[i].prueba);
        printf("\tinserte el tiempo en formado (M,S): ");
        scanf("%d,%d",&nadadores[i].tiempo.minutos,&nadadores[i].tiempo.segundos);
    }

    return;
}

int tiempo_promedioM (nnadadores nadadores[], int tamano)
{
    int promedio=0, i;
    for(i=0;i<tamano;i++)
        promedio+=nadadores[i].tiempo.minutos;

    return promedio;

}

int tiempo_promedioS (nnadadores nadadores[], int tamano)
{
    int promedio=0, i;
    for(i=0;i<tamano;i++)
        promedio+=nadadores[i].tiempo.segundos;

    return promedio;
}
