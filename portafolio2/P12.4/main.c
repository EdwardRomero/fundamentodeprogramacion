#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define E 2.718
double f(int x);
double g(int x);
double z(int x);

int main()
{
    printf("Este programa evalua funciones dadas\n");
    float contador;
    for(contador=0;contador<3.5;contador+=0.2)
    {
        printf("con %f la funcion f(x) da como resultado %f\n", contador, f(contador));
        printf("con %f la funcion g(x) da como resultado %f\n", contador, g(contador));
        printf("con %f la funcion z(x) da como resultado %f\n", contador, z(contador));
        printf("\n\n");
    }
    return 0;
}

double f(int x)
{
    return 3*pow(E,x)-2*x;
}

double g(int x)
{
   return sin(x)*(x*-1)+1.5;
}

double z(int x)
{
   return pow(x,2)-(2*x)+3;
}
